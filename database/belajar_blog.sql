-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 12 Nov 2020 pada 01.19
-- Versi server: 10.3.16-MariaDB
-- Versi PHP: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `belajar_blog`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `blog`
--

CREATE TABLE `blog` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `category` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `blog`
--

INSERT INTO `blog` (`id`, `title`, `content`, `created_at`, `updated_at`, `deleted_at`, `category`) VALUES
(1, '7 Makanan Tradisional Khas Jawa yang Cocok Disajikan saat HUT RI ke-75', 'Jakarta - Makanan tradisional khas Jawa pilihannya sangat beragam. Semua makanan ini memiliki makna spesial sehingga jadi sajian populer saat perayaan-perayaan tertentu, termasuk pada Hari Kemerdekaan HUT RI yang ke-75 ini.', NULL, NULL, NULL, 'HUT RI Ke-75'),
(2, 'test', 'test', NULL, NULL, NULL, 'test'),
(3, 'Dr.', 'Ea dolorem et placeat adipisci ad ut. Natus ab voluptatibus et hic. Alias quam similique accusamus itaque nobis occaecati.', '2020-08-18 16:33:19', '2020-08-18 16:33:19', NULL, 'Biological Technician'),
(4, 'Mrs.', 'Odio tempore qui est laboriosam vel nesciunt repellat. Ex perspiciatis velit quidem rem. Minima aut quia assumenda est.', '2020-08-18 16:33:19', '2020-08-18 16:33:19', NULL, 'Fabric Mender'),
(5, 'Dr.', 'Laboriosam magnam aut ea voluptatem quam quod. Accusamus et aliquam sed et architecto. Et nihil esse ab. Dignissimos qui itaque esse autem sunt est labore.', '2020-08-18 16:33:19', '2020-08-18 16:33:19', NULL, 'Poultry Cutter'),
(6, 'Mr.', 'Voluptate ut quisquam eveniet velit officia eligendi. Et labore atque omnis. Nobis est odit est neque voluptatem at. Ut facilis enim repellat.', '2020-08-18 16:33:19', '2020-08-18 16:33:19', NULL, 'Directory Assistance Operator'),
(7, 'Prof.', 'Perferendis quia laudantium natus est. Neque minima maxime possimus veniam. Excepturi et quasi tempore architecto voluptatem nam.', '2020-08-18 16:33:20', '2020-08-18 16:33:20', NULL, 'Biological Technician'),
(8, 'Dr.', 'Atque voluptatum non voluptatem est velit. Consequuntur voluptates repellat dolores et.', '2020-08-18 16:33:20', '2020-08-18 16:33:20', NULL, 'Architect'),
(9, 'Prof.', 'Consequatur voluptatum perferendis consequatur ipsum delectus et. Quas sit aperiam eligendi eligendi.', '2020-08-18 16:33:20', '2020-08-18 16:33:20', NULL, 'Public Transportation Inspector'),
(10, 'Mrs.', 'Pariatur consequatur nesciunt dolorem odit quo voluptatem velit sapiente. Velit omnis culpa numquam qui id. Ut sit ex non deserunt consequuntur vero accusamus. Itaque voluptatem aut repellendus quaerat.', '2020-08-18 16:33:20', '2020-08-18 16:33:20', NULL, 'Mechanical Inspector'),
(11, 'Ms.', 'Voluptatem odit quo modi et molestiae. Inventore esse quidem recusandae sapiente voluptas molestiae eos.', '2020-08-18 16:33:20', '2020-08-18 16:33:20', NULL, 'Material Movers'),
(12, 'Prof.', 'Ullam enim dolores dolorem nisi itaque. Voluptatem quia error praesentium commodi ipsum sint cupiditate aliquam. Vel esse blanditiis quam laborum autem delectus qui. Neque est hic quidem necessitatibus est fuga aperiam. Architecto recusandae modi inventore enim itaque.', '2020-08-18 16:33:20', '2020-08-18 16:33:20', NULL, 'Hand Trimmer'),
(13, 'Prof.', 'Quae voluptatem voluptatem aut sunt nobis maiores quia. Non facere voluptatem totam et non laborum. Exercitationem culpa et autem. Sed non eveniet non natus eum molestias.', '2020-08-18 16:33:20', '2020-08-18 16:33:20', NULL, 'Retail Sales person'),
(14, 'Miss', 'Deleniti molestiae corrupti aperiam culpa eveniet ad voluptatem. Qui sapiente voluptatem dolor quibusdam. Vero nostrum aliquam sint delectus vel adipisci ut.', '2020-08-18 16:33:20', '2020-08-18 16:33:20', NULL, 'Geographer'),
(15, 'Ms.', 'Et quo praesentium vel et iusto quis doloribus molestias. Est nesciunt sed omnis rem non quos. Odit velit eum veritatis. Quod rem ratione consectetur et explicabo non molestias.', '2020-08-18 16:33:20', '2020-08-18 16:33:20', NULL, 'Social Scientists'),
(16, 'Prof.', 'Molestiae voluptatibus et suscipit eligendi a. At debitis quo commodi labore aut.', '2020-08-18 16:33:20', '2020-08-18 16:33:20', NULL, 'Fire-Prevention Engineer'),
(17, 'Mrs.', 'Nesciunt beatae soluta eos est accusamus quo. Explicabo et dolores assumenda sed itaque nesciunt et. Veniam qui sed consequuntur blanditiis nobis ipsa. Et a accusamus perspiciatis aut soluta impedit porro. Natus id maiores omnis dolorem deleniti.', '2020-08-18 16:33:20', '2020-08-18 16:33:20', NULL, 'Aerospace Engineer'),
(18, 'Mr.', 'Ut enim voluptas harum voluptatem debitis ratione laudantium. Impedit assumenda minima aut voluptatibus eos. Quia velit maxime quisquam voluptates laudantium dolorem maiores.', '2020-08-18 16:33:20', '2020-08-18 16:33:20', NULL, 'Marine Cargo Inspector'),
(19, 'Prof.', 'Non voluptatem id sed assumenda rerum accusamus vero. Dicta numquam repellendus deleniti consequuntur perspiciatis deserunt. Vel facere occaecati sint ipsum. Consequatur eos dolorem cupiditate voluptatem natus illo in.', '2020-08-18 16:33:21', '2020-08-18 16:33:21', NULL, 'Paste-Up Worker'),
(20, 'Ms.', 'Non molestias dolore eligendi vitae in a. Delectus est maiores ut eum ea asperiores eum illo. Labore id ut libero maxime. Illum dolor consequuntur occaecati dolorem rerum nulla.', '2020-08-18 16:33:21', '2020-08-18 16:33:21', NULL, 'Shoe and Leather Repairer'),
(21, 'Dr.', 'Eveniet dolorem dolores reprehenderit velit. Nemo ut et quia aut aliquid qui ut. Praesentium magnam est minus eligendi. Sapiente voluptatum fugiat quo sunt repellat eos.', '2020-08-18 16:33:21', '2020-08-18 16:33:21', NULL, 'Optical Instrument Assembler'),
(22, 'Mrs.', 'Dolores ducimus facere alias quis iusto. Quod ipsum corporis molestiae sit. Impedit dolores vel illum voluptatem soluta vel. Impedit voluptatibus aut distinctio iure qui sint.', '2020-08-18 16:33:21', '2020-08-18 16:33:21', NULL, 'Welding Machine Tender'),
(23, 'Dr.', 'Molestiae sunt excepturi iste incidunt autem officiis vel. Laborum distinctio officia provident reprehenderit quaerat. Iste asperiores dolore dolore neque rem sunt assumenda.', '2020-08-18 16:33:21', '2020-08-18 16:33:21', NULL, 'Equal Opportunity Representative'),
(24, 'Miss', 'Quibusdam ea corporis aliquid officiis. Qui repellat dolore sed quae sed soluta quae. Molestiae quia soluta eligendi et aut consequuntur. Iusto odit libero est est molestiae qui.', '2020-08-18 16:33:21', '2020-08-18 16:33:21', NULL, 'Motorcycle Mechanic'),
(25, 'Dr.', 'Quis debitis ducimus et nobis soluta necessitatibus est. Accusamus recusandae enim sint autem qui illo eveniet deserunt. Illum non voluptatem ut numquam deleniti.', '2020-08-18 16:33:21', '2020-08-18 16:33:21', NULL, 'Medical Secretary'),
(26, 'Mr.', 'Amet dignissimos odio accusamus molestiae iste recusandae dolores. Enim modi repellendus at et eos esse. Ut est soluta voluptatem temporibus voluptate debitis omnis.', '2020-08-18 16:33:21', '2020-08-18 16:33:21', NULL, 'Precision Lens Grinders and Polisher'),
(27, 'Prof.', 'Atque est consequatur iusto ut aut. Eligendi minima fugiat nostrum vel velit. Et recusandae similique iusto dignissimos aspernatur ex. Est numquam cum ipsa ut sunt ipsum. Illum aut eius et eum.', '2020-08-18 16:33:21', '2020-08-18 16:33:21', NULL, 'Bicycle Repairer'),
(28, 'Mr.', 'Iste et rerum illum aut distinctio hic et odit. Quis neque pariatur dolore sit aut. Aspernatur consequatur esse voluptatem omnis. Nemo quidem exercitationem recusandae aut provident voluptatem officia.', '2020-08-18 16:33:21', '2020-08-18 16:33:21', NULL, 'Production Control Manager'),
(29, 'Mr.', 'Est dolorem accusamus vitae et. Maxime est soluta culpa sunt magni eos provident. Provident illum culpa asperiores et et quis praesentium.', '2020-08-18 16:33:21', '2020-08-18 16:33:21', NULL, 'Court Reporter'),
(30, 'Ms.', 'Quia modi aut quibusdam tenetur. Natus fugiat similique fuga hic eos est deserunt modi. Et sed qui quas. Consequatur ut harum nostrum quisquam et consequuntur rerum nostrum.', '2020-08-18 16:33:21', '2020-08-18 16:33:21', NULL, 'Gas Pumping Station Operator'),
(31, 'Dr.', 'Possimus iusto harum et adipisci velit harum soluta. Molestiae et quam eos omnis consectetur libero. Quod delectus aut eos facere velit.', '2020-08-18 16:33:22', '2020-08-18 16:33:22', NULL, 'Mail Clerk'),
(32, 'Mr.', 'Sit officiis esse rerum. Neque sint aliquid explicabo ea veniam quae. Provident velit rerum vitae rerum dicta eos blanditiis veniam.', '2020-08-18 16:33:22', '2020-08-18 16:33:22', NULL, 'Eligibility Interviewer'),
(33, 'Ms.', 'Voluptatem id iusto quia ut enim mollitia consectetur. Eligendi placeat dolorem corporis omnis. Eveniet voluptatem qui accusantium est. Quia amet rerum consequatur quaerat eaque. Rerum minus aut alias labore ut sed.', '2020-08-18 16:33:22', '2020-08-18 16:33:22', NULL, 'Electrical and Electronic Inspector and Tester'),
(34, 'Mr.', 'Quia magnam maxime fuga eum. Ipsum quia temporibus ut autem. Eius dolorum saepe cumque temporibus porro quibusdam odit et.', '2020-08-18 16:33:22', '2020-08-18 16:33:22', NULL, 'Host and Hostess'),
(35, 'Mrs.', 'Sunt similique corporis et ullam consectetur rem facilis. Impedit quasi beatae libero. Non saepe doloribus dolores atque aut. Ea iste recusandae consequatur.', '2020-08-18 16:33:22', '2020-08-18 16:33:22', NULL, 'Social Work Teacher'),
(36, 'Prof.', 'Ut magnam veritatis dolor. Minus illo blanditiis distinctio harum officia dignissimos. Nobis eaque non accusamus quae amet voluptas cumque.', '2020-08-18 16:33:22', '2020-08-18 16:33:22', NULL, 'Title Searcher'),
(37, 'Prof.', 'Mollitia libero magni fugiat eos et neque porro. Consequatur ipsum consequatur et autem. Ratione dolor vero id. Et aut et harum enim dolor rerum ad.', '2020-08-18 16:33:22', '2020-08-18 16:33:22', NULL, 'Timing Device Assemblers'),
(38, 'Miss', 'Minus nihil ratione similique dignissimos. Esse sequi beatae qui provident et. Et suscipit itaque et commodi libero ducimus. Impedit odit sequi animi aut.', '2020-08-18 16:33:22', '2020-08-18 16:33:22', NULL, 'Septic Tank Servicer'),
(39, 'Mrs.', 'Est consequatur occaecati modi enim autem nihil. Doloribus provident sit recusandae doloremque non. Autem et in alias omnis. Eaque maxime qui tempora totam vitae enim at.', '2020-08-18 16:33:22', '2020-08-18 16:33:22', NULL, 'Auditor'),
(40, 'Prof.', 'Sequi rerum vero officiis officiis iusto culpa. Veniam magnam est modi ducimus qui ut. Est delectus modi facilis est laboriosam.', '2020-08-18 16:33:22', '2020-08-18 16:33:22', NULL, 'Animal Breeder'),
(41, 'Dr.', 'Eaque dignissimos nulla explicabo est. Qui laudantium et consequatur. Quia beatae sit commodi possimus et.', '2020-08-18 16:33:22', '2020-08-18 16:33:22', NULL, 'Healthcare Practitioner'),
(42, 'Prof.', 'Itaque a rerum sint culpa sint odio assumenda voluptatum. Cum delectus veniam qui quas blanditiis natus. Est placeat facere qui autem ullam corrupti recusandae.', '2020-08-18 16:33:22', '2020-08-18 16:33:22', NULL, 'Public Relations Manager'),
(43, 'Mr.', 'Ab reiciendis omnis voluptas quis ex et. Dolor esse nemo laboriosam id. Fuga id aperiam impedit asperiores.', '2020-08-18 16:33:23', '2020-08-18 16:33:23', NULL, 'Chemist'),
(44, 'Mrs.', 'Quae laborum nemo et. Itaque omnis nisi ipsa incidunt sint possimus. Et quasi non rerum ratione voluptas ipsam ut. Facilis nostrum quisquam odit.', '2020-08-18 16:33:23', '2020-08-18 16:33:23', NULL, 'Computer Repairer'),
(45, 'Prof.', 'Quis id quibusdam rerum eaque magni fugiat aut. Maiores qui nesciunt suscipit deserunt nemo sunt.', '2020-08-18 16:33:23', '2020-08-18 16:33:23', NULL, 'Human Resources Manager'),
(46, 'Prof.', 'Explicabo in earum sed. Est ut facere sed veniam. Et est quam amet a.', '2020-08-18 16:33:23', '2020-08-18 16:33:23', NULL, 'Protective Service Worker'),
(47, 'Mrs.', 'Corporis id architecto iure quos nobis non officia. Molestias quo nam quis ex magni. Quia sit dolore est inventore.', '2020-08-18 16:33:23', '2020-08-18 16:33:23', NULL, 'Wind Instrument Repairer'),
(48, 'Prof.', 'Est eum ut fuga voluptatem id nihil explicabo. Voluptatum nisi officiis voluptatibus atque itaque quasi. Qui nemo aut est molestiae et. Molestiae qui consequatur at eius veritatis.', '2020-08-18 16:33:23', '2020-08-18 16:33:23', NULL, 'Securities Sales Agent'),
(49, 'Mrs.', 'Amet error quidem modi aut provident. Sapiente minus pariatur earum. Quas qui tenetur vel doloribus ipsam a sit. Unde aut et atque aut a aut occaecati. Facere repellendus qui saepe voluptas ea nostrum distinctio.', '2020-08-18 16:33:23', '2020-08-18 16:33:23', NULL, 'Fitter'),
(50, 'Mrs.', 'Quidem vero animi totam saepe ducimus. Et quae et suscipit unde aut aut voluptatem. Ut autem qui dolores ut.', '2020-08-18 16:33:23', '2020-08-18 16:33:23', NULL, 'Textile Cutting Machine Operator'),
(51, 'Mr.', 'Et recusandae omnis delectus quis. Eveniet commodi laboriosam maxime et qui laboriosam. At sit nam facere ut assumenda repellendus facere.', '2020-08-18 16:33:23', '2020-08-18 16:33:23', NULL, 'Transportation Equipment Painters'),
(52, 'Miss', 'Sunt id aliquid sequi amet. Provident commodi aut voluptatem nihil. Earum maiores voluptatum molestiae. Magnam sit et accusantium nulla repellendus est occaecati.', '2020-08-18 16:33:23', '2020-08-18 16:33:23', NULL, 'Employment Interviewer'),
(53, 'Miss', 'Velit molestias perspiciatis sapiente sunt provident. Architecto et voluptas beatae. Voluptate velit amet aspernatur quae sed et. Odio qui voluptatum earum.', '2020-08-18 16:33:23', '2020-08-18 16:33:23', NULL, 'Licensed Practical Nurse'),
(54, 'Mrs.', 'Rem in consequuntur nam accusamus maiores molestiae qui nihil. Molestiae natus sit et pariatur. Quia itaque sint ab et.', '2020-08-18 16:33:23', '2020-08-18 16:33:23', NULL, 'Fish Game Warden'),
(55, 'Dr.', 'Exercitationem soluta voluptate qui aut. Et vero atque ex et labore. Sit amet nisi molestiae.', '2020-08-18 16:33:24', '2020-08-18 16:33:24', NULL, 'Environmental Science Teacher'),
(56, 'Ms.', 'Quo rerum suscipit ipsa vel. Ut incidunt sit non et rem quidem blanditiis. Vel voluptas autem quia architecto.', '2020-08-18 16:33:24', '2020-08-18 16:33:24', NULL, 'Anthropology Teacher'),
(57, 'Dr.', 'Est dolorem voluptatem itaque minus qui eaque. Non expedita ducimus veritatis excepturi odit reprehenderit adipisci et. Facere doloremque nihil ratione reiciendis.', '2020-08-18 16:33:24', '2020-08-18 16:33:24', NULL, 'Loading Machine Operator'),
(58, 'Prof.', 'At fugit nihil quis est dolorem dolorem. Aliquam velit sunt distinctio. Laudantium odit corrupti placeat consequatur quam. Sapiente atque enim rerum aut reprehenderit nobis voluptatem nesciunt. Dolorum non expedita delectus eligendi.', '2020-08-18 16:33:24', '2020-08-18 16:33:24', NULL, 'Stonemason'),
(59, 'Mr.', 'Provident maxime similique dolorem et et. Est sed quis quasi. Enim illum beatae deserunt fuga. Accusantium ut rerum veniam dolorum ut voluptatum ut sapiente.', '2020-08-18 16:33:24', '2020-08-18 16:33:24', NULL, 'Court Reporter'),
(60, 'Mrs.', 'Rerum reprehenderit porro aut libero rem culpa distinctio. Nobis aliquid id beatae vel quia error qui. Voluptatibus quam vel nobis voluptas sed.', '2020-08-18 16:33:24', '2020-08-18 16:33:24', NULL, 'Agricultural Product Grader Sorter'),
(61, 'Prof.', 'Cum reiciendis voluptatem doloremque magni. Id labore officia officia molestiae fugiat. Corrupti voluptas cupiditate sequi dolorem libero. Praesentium tempore commodi sint ut cumque reiciendis doloribus.', '2020-08-18 16:33:24', '2020-08-18 16:33:24', NULL, 'Wholesale Buyer'),
(62, 'Mr.', 'Eligendi officia eum nisi asperiores. Sequi et dolores ipsum dignissimos enim quis alias. Et dolor saepe est sequi modi qui. Ducimus possimus voluptas voluptas dolores soluta possimus aut.', '2020-08-18 16:33:24', '2020-08-18 16:33:24', NULL, 'Numerical Tool Programmer OR Process Control Programmer'),
(63, 'Miss', 'Sapiente illum suscipit nihil eos illo. Expedita voluptate libero harum. Tenetur molestiae hic quaerat voluptatum.', '2020-08-18 16:33:24', '2020-08-18 16:33:24', NULL, 'Alteration Tailor'),
(64, 'Dr.', 'Aliquid et earum et maxime ut. Qui explicabo distinctio ex qui beatae id omnis.', '2020-08-18 16:33:24', '2020-08-18 16:33:24', NULL, 'Grinder OR Polisher'),
(65, 'Prof.', 'Architecto omnis rerum tenetur. Repudiandae perspiciatis et dolorem quasi veniam eos sed aut. Ab quos voluptatem rem incidunt perspiciatis. Quo nihil sit aut provident id commodi sit.', '2020-08-18 16:33:24', '2020-08-18 16:33:24', NULL, 'Gas Plant Operator'),
(66, 'Miss', 'Aperiam corrupti vero ipsam vero saepe. Ea voluptas pariatur velit possimus accusantium ipsa sint maxime.', '2020-08-18 16:33:24', '2020-08-18 16:33:24', NULL, 'Psychology Teacher'),
(67, 'Ms.', 'Impedit enim voluptatem libero consequuntur. Ex dolore molestiae vero ea est. Commodi velit aut pariatur molestiae adipisci. Quod eius tenetur qui a dolores et maiores sit. Qui ea est laborum fuga quibusdam rem officiis at.', '2020-08-18 16:33:24', '2020-08-18 16:33:24', NULL, 'Marine Oiler'),
(68, 'Prof.', 'Eius sapiente veritatis omnis et doloribus enim. Vero voluptatum tenetur et incidunt. Quia itaque temporibus beatae officiis harum sit similique architecto. Qui veniam veritatis atque tempore reprehenderit.', '2020-08-18 16:33:24', '2020-08-18 16:33:24', NULL, 'Soldering Machine Setter'),
(69, 'Dr.', 'Perspiciatis doloribus et autem aperiam voluptatem dicta. Deserunt quia commodi molestiae ea et et similique. Cum quas rem voluptas aliquam est. Et fuga et beatae sed in.', '2020-08-18 16:33:24', '2020-08-18 16:33:24', NULL, 'Network Admin OR Computer Systems Administrator'),
(70, 'Miss', 'Consequatur nulla eveniet earum et inventore assumenda. Ut distinctio reiciendis incidunt quasi pariatur. Laborum consequuntur quae dicta. Sunt ratione unde sit et occaecati omnis facere.', '2020-08-18 16:33:25', '2020-08-18 16:33:25', NULL, 'Business Teacher'),
(71, 'Prof.', 'Nemo impedit impedit perspiciatis nam quam. Quo rem et sint. Sit earum voluptatem aut.', '2020-08-18 16:33:25', '2020-08-18 16:33:25', NULL, 'Instrument Sales Representative'),
(72, 'Prof.', 'Nostrum nostrum iste optio qui. Architecto dicta quis dolorem. Ea aperiam tempore magni doloribus.', '2020-08-18 16:33:25', '2020-08-18 16:33:25', NULL, 'Landscaping'),
(73, 'Mr.', 'Voluptas voluptatem deleniti laboriosam alias dolorum quia voluptates. Consequuntur illum minima et ut rerum illo et totam.', '2020-08-18 16:33:25', '2020-08-18 16:33:25', NULL, 'Offset Lithographic Press Operator'),
(74, 'Mrs.', 'Voluptatem accusantium dignissimos unde iure maxime consequatur et fugit. Ad sunt aut eveniet sed.', '2020-08-18 16:33:25', '2020-08-18 16:33:25', NULL, 'Central Office'),
(75, 'Miss', 'Tenetur commodi alias et sit. Rerum nobis consequatur consectetur suscipit impedit ut. Temporibus non repellendus nam maiores hic facilis asperiores.', '2020-08-18 16:33:25', '2020-08-18 16:33:25', NULL, 'Aerospace Engineer'),
(76, 'Dr.', 'Est minima corporis harum amet. Alias iste provident id repellendus pariatur. Omnis consequuntur et dignissimos illo dicta. Accusamus optio qui voluptatem illum sint velit qui.', '2020-08-18 16:33:25', '2020-08-18 16:33:25', NULL, 'Music Composer'),
(77, 'Mr.', 'Quo reprehenderit cum magni quis illum. Rerum architecto labore repellat dolor. Voluptas dolorum placeat est voluptatum. Et ab dolores et sint.', '2020-08-18 16:33:25', '2020-08-18 16:33:25', NULL, 'Public Health Social Worker'),
(78, 'Mr.', 'Cum earum ut provident libero. Ut minima voluptatem earum et. Quas temporibus rerum enim et.', '2020-08-18 16:33:25', '2020-08-18 16:33:25', NULL, 'Glass Blower'),
(79, 'Mr.', 'Et ut optio dolorem sit. Dolores voluptatibus est magnam voluptatem. Ullam consectetur nostrum qui ab aut reprehenderit repellat sed. Quam et dolor sint atque repudiandae.', '2020-08-18 16:33:25', '2020-08-18 16:33:25', NULL, 'Lodging Manager'),
(80, 'Prof.', 'Hic sunt eaque atque fugit earum voluptate et. Numquam perferendis architecto non qui nihil. Nihil modi alias error consequuntur eos. Provident sit quo est velit.', '2020-08-18 16:33:25', '2020-08-18 16:33:25', NULL, 'Stringed Instrument Repairer and Tuner'),
(81, 'Mr.', 'Numquam amet earum consequatur soluta eum et eos. Iusto quia inventore quisquam tempora. Sequi amet quos quae suscipit. Doloribus quia veniam ut dolor autem qui velit.', '2020-08-18 16:33:25', '2020-08-18 16:33:25', NULL, 'Government'),
(82, 'Prof.', 'Quod nemo corporis inventore sint aliquam et ut. Quo et quod officiis voluptatem. Perspiciatis quis non nemo delectus.', '2020-08-18 16:33:25', '2020-08-18 16:33:25', NULL, 'Cashier'),
(83, 'Dr.', 'Cum dolorum voluptatem dolor nesciunt quas neque. Commodi quisquam reprehenderit ducimus qui repellat quis illo aliquam. Consequatur nulla optio porro iure.', '2020-08-18 16:33:25', '2020-08-18 16:33:25', NULL, 'Sales Person'),
(84, 'Ms.', 'Vel ad facere qui libero quod ut ullam. Earum quae voluptatem voluptatibus dignissimos aut ullam est. Omnis et consequatur eos delectus placeat est ut ut. Eos aliquam eos distinctio excepturi.', '2020-08-18 16:33:26', '2020-08-18 16:33:26', NULL, 'Heat Treating Equipment Operator'),
(85, 'Mr.', 'Distinctio ratione vero voluptatem ut quis. Deserunt est aut provident doloremque quia at ut minima. Impedit dolores possimus autem facilis eligendi voluptate. Impedit corporis doloremque culpa tempora.', '2020-08-18 16:33:26', '2020-08-18 16:33:26', NULL, 'Health Educator'),
(86, 'Prof.', 'Necessitatibus odio cum sit praesentium incidunt. Magnam cumque velit eos tempore et. Perspiciatis molestiae velit molestias sed est. Vitae aspernatur similique minima placeat est quaerat deleniti.', '2020-08-18 16:33:26', '2020-08-18 16:33:26', NULL, 'Floor Layer'),
(87, 'Mr.', 'Eveniet id amet nisi consequatur. Molestiae sequi numquam facilis labore porro tempore praesentium. Aut nisi numquam sed adipisci. Accusantium voluptatem ipsa aut natus officiis expedita.', '2020-08-18 16:33:26', '2020-08-18 16:33:26', NULL, 'Medical Secretary'),
(88, 'Mr.', 'Dolor possimus voluptatem est harum rerum enim rerum. Molestias sint labore reiciendis eaque magni. Vel quis voluptatem molestiae et doloribus sapiente nisi quae. Dolores magni eveniet iusto et.', '2020-08-18 16:33:26', '2020-08-18 16:33:26', NULL, 'Library Technician'),
(89, 'Ms.', 'Eos voluptatem atque nemo quam praesentium quia. Excepturi blanditiis quia aut eos alias sit veritatis. Perspiciatis exercitationem aliquam aut et deleniti sed rem. Officiis labore molestias sed labore molestias sunt tempora.', '2020-08-18 16:33:26', '2020-08-18 16:33:26', NULL, 'Database Manager'),
(90, 'Dr.', 'Accusantium dolor unde est nemo. Voluptates impedit voluptatum et dicta modi ad enim.', '2020-08-18 16:33:27', '2020-08-18 16:33:27', NULL, 'GED Teacher'),
(91, 'Dr.', 'Id a libero ut suscipit esse suscipit. Eligendi ut explicabo vel fugiat. Dolor ab voluptatem iste est reprehenderit provident. Praesentium dolorem ipsam enim id sapiente in magni. Impedit sint architecto excepturi dolores.', '2020-08-18 16:33:27', '2020-08-18 16:33:27', NULL, 'Order Filler OR Stock Clerk'),
(92, 'Miss', 'Optio occaecati mollitia deserunt sit soluta vel corporis. Aut ea dolorum autem earum. Dicta animi ut et voluptatum autem assumenda.', '2020-08-18 16:33:27', '2020-08-18 16:33:27', NULL, 'Credit Checkers Clerk'),
(93, 'Prof.', 'Quia tempora id sed reiciendis voluptatem repellat numquam. Et omnis eum necessitatibus quaerat rerum. Dolorem neque atque quo id explicabo accusantium quaerat rem.', '2020-08-18 16:33:27', '2020-08-18 16:33:27', NULL, 'Transportation Worker'),
(94, 'Mr.', 'Officia nemo libero ipsum eos facilis enim natus aliquam. Ea a beatae dolorem dolores. Corrupti aliquam enim ea animi sapiente.', '2020-08-18 16:33:27', '2020-08-18 16:33:27', NULL, 'Recreational Therapist'),
(95, 'Miss', 'Ea consequatur quam voluptatem mollitia et placeat. Non beatae dolorem saepe accusantium dolor. Quos sapiente molestiae quia recusandae reiciendis quod. Architecto distinctio eius earum ad omnis excepturi dolor.', '2020-08-18 16:33:27', '2020-08-18 16:33:27', NULL, 'Software Engineer'),
(96, 'Dr.', 'Dicta in quia maxime sequi occaecati illum. Aut perferendis consequatur magni id est illo quis. Voluptatum dolores quia sit odio iste cupiditate sequi.', '2020-08-18 16:33:27', '2020-08-18 16:33:27', NULL, 'Material Movers'),
(97, 'Mr.', 'Sed quo incidunt aut est. Ipsum repellendus unde natus ut repudiandae. Tempore non nesciunt blanditiis ducimus amet molestiae et.', '2020-08-18 16:33:27', '2020-08-18 16:33:27', NULL, 'Education Teacher'),
(98, 'Ms.', 'Quis quam amet rem eos nesciunt atque qui. Culpa quis facere nostrum sunt enim. Et eos quo neque. Blanditiis odio hic molestias voluptates omnis cumque illo.', '2020-08-18 16:33:27', '2020-08-18 16:33:27', NULL, 'Archivist'),
(99, 'Prof.', 'Ut est aliquid voluptatem reiciendis maiores aut. Ut corrupti ea aut vel vitae ipsum. Aut eos inventore enim quas. Blanditiis recusandae rerum ut.', '2020-08-18 16:33:28', '2020-08-18 16:33:28', NULL, 'Engineering Teacher'),
(100, 'Prof.', 'Nemo molestiae tempore dolorum id. Praesentium praesentium error laborum et nam. Et aspernatur possimus cum cupiditate hic. Dolorum aliquid molestias accusamus dicta aut.', '2020-08-18 16:33:28', '2020-08-18 16:33:28', NULL, 'Food Tobacco Roasting'),
(101, 'Dr.', 'Velit vel consectetur suscipit sed. Tenetur ipsum et consequatur fuga quasi repellat. Ut iure ex minus ad labore iusto autem.', '2020-08-18 16:33:28', '2020-08-18 16:33:28', NULL, 'Sheet Metal Worker'),
(102, 'Dr.', 'Id esse aut facilis illo ut sed. Qui in quo qui aliquid libero nisi.', '2020-08-18 16:33:28', '2020-08-18 16:33:28', NULL, 'Electrician');

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(3, '2020_08_11_230913_create_blog_table', 1),
(4, '2020_08_11_231927_add_category_in_blog_table', 1);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
